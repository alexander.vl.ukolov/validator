object ViewExtensions {

    /**
     * Метод-расширение для класса [View] для получения списка доступных компонентов
     * для фокусировки.
     *
     * @param T Тип компонента для осуществления выборки.
     *
     * @return Вернет список компонентов типа [T].
     */
    inline fun <reified T : View> View.getFocusableComponents(): List<T> =
        Stream.of(getFocusables(View.FOCUS_FORWARD))
            .filter { it is T }
            .map { it as T }
            .toList()

    /**
     * Возвращает tag [View] как [Int].
     *
     * @return Если tag можно преобразовать в [Int] то вернется число, иначе null.
     */
    fun View.getTagAsInt(): Int? =
        try {
            this.tag.toString().toInt()
        } catch (e: NullPointerException) {
            e.printStackTrace()
            null
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            null
        }
}
