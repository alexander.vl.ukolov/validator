object Validators {

    private const val MOBILE_PHONE_PATTERN = "\\+7(?: \\([0-9]{3}\\)) (?:[0-9]{3})(?: [0-9]{2}){2}"
    private const val PASSWORD_VALIDATOR_PATTERN = "\\s"

    /**
     * Метод осуществляет валидацию телефонного номера мобильного утсройства.
     *
     * @param phoneNumber Телефонный номер.
     * Должен приходить в формате +7 (***) *** ** **.
     *
     * @return Если все хорошо, то вернется true, иначе false.
     */
    fun validatePhoneNumber(phoneNumber: String?): Boolean {
        val pattern = Pattern.compile(MOBILE_PHONE_PATTERN)
        return phoneNumber != null && pattern.matcher(phoneNumber).matches()
    }

    /**
     * Метод осуществляет валидацию пароля.
     *
     * @param password Пароль.
     *
     * @return Если пароль содержит пробелы или пустой, то вернется false, иначе true.
     */
    fun validatePassword(password: String?) =
        password?.run { !(length < 8 || contains(Regex(PASSWORD_VALIDATOR_PATTERN))) } ?: false

    /**
     * Метод осуществляет валидацию по пустомы шаблону.
     *
     * @param text Текст, который проверяется на пустоту.
     *
     * @return Если текст пуст то вернет false, иначе true.
     */
    fun validateEmpty(text: String?) = !TextUtils.isEmpty(text)
}
