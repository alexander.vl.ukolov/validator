object FragmentExtensions {

    /**
     * Метод-расширение для класса [Fragment] для валидации текста в компонентах типа
     * [AppCompatEditText]. Каждое проверяемое текстовое поле должно иметь tag от нуля до того
     * количества валидаторов, которое планируется использовать. Tag у объектов может повторяться,
     * тогда к ним будет применен один и тот же валидатор.
     *
     * @return Вернет true, если валидация прошла успешно или false, если не прошла.
     */
    fun Fragment.validateInput(): Boolean {
        var validated = true

        // проверяем, реализует ли фрагмент интерфейс Validator
        if (this is Validator) {

            // получаем список валидаторов и ссылок на строковые ресурсы с текстами ошибок
            val validators = getValidators()
            val errorTextResources = getErrorTextResources()

            view?.let {

                // получаем компоненты, которые можно выбрать и перебираем их для осуществления
                // контроля введенного значения
                val components = it.getFocusableComponents<AppCompatEditText>()
                components.forEach { component ->

                    // получаем индекс по тегу объекта для определения валидатора и текста с
                    // описанием ошибки
                    val index = (component as View).getTagAsInt() ?: return@forEach

                    // получаем валидатор и ссылку на строковый ресурс с текстом ошибки
                    val validator = validators[index]
                    val errorTextResource = errorTextResources[index]

                    // проверяем значение текстового поля
                    val validatedComponent = validator(component.text.toString())

                    // показываем или скрываем сообщение с текстом ошибки, фиксируем не прохождение
                    // проверки, если хотя бы один из компонентов ее не прошел
                    val parent = component.parent.parent as TextInputLayout
                    if (validatedComponent) {
                        parent.isErrorEnabled = false
                    } else {
                        parent.error = getString(errorTextResource)
                        parent.isErrorEnabled = true
                        validated = false
                    }
                }
            }
        }

        return validated
    }
}